# Foco



## General

El proyecto puede exportarse para 64 bits si se hace con la librería `pyinstaller` desde una instalación de python para 64 bits.

Se puede generar un entorno virtual con python 3.7.9 por si se deseara exportar una ueva versión para 32 bits.

## Librerías necesarias
Las librerías que se utilizan son:
• `customtkinter`
• `matplotlib` (se usa la versión 2.2.2 en el entron virtual ya que el resto presentan fallas)
• `pygame`
• `pyinstaller` (para generar el .exe)
